prefix imf: <http://example.com/imf#> 
prefix lis: <http://standards.iso.org/iso/15926/part14/> 
prefix owl: <http://www.w3.org/2002/07/owl#> 
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
prefix sor: <https://rdf.equinor.com/sor/mimir/> 
prefix xml: <http://www.w3.org/XML/1998/namespace> 
prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
prefix mimir: <http://example.com/mimir#> 


# Sjekker at når to terminaler er koplet sammen, er de av samme type transmitter
#select ?node1 ?qTypeName ?v1 ?node2 ?v2 where { 
select * where {
    ?t1 imf:connectedTo ?t2;
        a ?type1;
        rdfs:label ?terminal1.
  	?type1 rdfs:subClassOf mimir:Transmitter.
    ?t2 a ?type2;
        rdfs:label ?terminal2.
    ?type2 rdfs:subClassOf mimir:Transmitter.
    FILTER(?type1 != mimir:Transmitter)
    FILTER(?type2 != mimir:Transmitter)
    FILTER(?type1 != ?type2).
}