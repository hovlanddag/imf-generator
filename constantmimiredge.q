prefix imf: <http://example.com/imf#> 
prefix lis: <http://standards.iso.org/iso/15926/part14/> 
prefix owl: <http://www.w3.org/2002/07/owl#> 
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
prefix sor: <https://rdf.equinor.com/sor/mimir/> 
prefix xml: <http://www.w3.org/XML/1998/namespace> 
prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
prefix mimir: <http://example.com/mimir#> 


# Sjekker at når to noder er koplet sammen vha. en transport, så er evt. attributer som er markert som ConstantPhysicalQuantity
# identiske på de to nodene. 
select ?node1 ?qTypeName ?v1 ?node2 ?v2 where { 
#select * where {
    ?n1 rdfs:label ?node1;  
        mimir:hasPurpose ?purp;  
        mimir:hasMimirEdge ?n2;  
        lis:hasPhysicalQuantity ?q1.
    
  ?q1 a ?qType,  
            mimir:ConstantPhysicalQuantity ;  
            mimir:isTransformedBy ?transf;  
            lis:qualityQuantifiedAs ?d1.  
    ?transf rdfs:label ?transformer.  
    FILTER NOT EXISTS {  
        ?purp mimir:transforms ?q1. 
    } . 
    ?qType rdfs:label ?qTypeName.  
    ?d1 lis:datumValue ?v1.  
    ?n2 rdfs:label ?node2.
    OPTIONAL{  
        ?n2 lis:hasPhysicalQuantity ?q2;  
            mimir:hasPurpose ?purp2. 
        ?q2 a ?qType;  
            lis:qualityQuantifiedAs ?d2.  
        ?d2 lis:datumValue ?v2.  
        FILTER(?v1 != ?v2).  
    } .
     FILTER NOT EXISTS {  
            ?purp2 mimir:transforms ?q2. 
    } . 
}