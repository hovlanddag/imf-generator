prefix imf: <http://ns.imfid.org/imf#> 
prefix lis: <http://standards.iso.org/iso/15926/part14/> 
prefix owl: <http://www.w3.org/2002/07/owl#> 
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
prefix sor: <https://rdf.equinor.com/sor/mimir/> 
prefix xml: <http://www.w3.org/XML/1998/namespace> 
prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
prefix mimir: <http://ns.imfid.org/mimir#> 


SELECT DISTINCT ?n1 ?rds1 ?n2 ?rds2 ?q1 ?q2 ?d1 ?d2 ?v1 ?v2 WHERE {
    ?n1 rdfs:label ?node1;
        imf:rds ?rds1;
        mimir:hasPurpose ?purp1;  
        imf:hasTerminal ?t1.
    ?t1 a imf:OutTerminal;
        imf:connectedTo/^imf:hasTerminal/imf:hasTerminal/imf:connectedTo/^imf:hasTerminal ?n2.  
    #?n1 mimir:hasMimirEdge ?n2.      
    FILTER (?n1 != ?n2).
    ?n1 a imf:FunctionalSystemBlock.
    ?n2 a imf:FunctionalSystemBlock.
    ?n1 lis:hasPhysicalQuantity ?q1.
    ?q1 a ?qType, mimir:ConstantPhysicalQuantity;
        lis:qualityQuantifiedAs ?d1.   
    FILTER NOT EXISTS {?purp1 mimir:transforms ?q1 } 
    ?qType rdfs:label ?qTypeName.  
    ?d1 lis:datumValue ?v1.  
    {
    ?n2 rdfs:label ?node2;
	       imf:rds ?rds2;
        mimir:hasPurpose ?purp2;
        lis:hasPhysicalQuantity ?q2. 
	FILTER NOT EXISTS {?purp2 mimir:transforms ?q2 }
	 ?q2 a ?qType;
	        lis:qualityQuantifiedAs ?d2.  
	 ?d2 lis:datumValue ?v2.  
	 FILTER(?v1 != ?v2).
} UNION {
    FILTER NOT EXISTS {
        ?n2 lis:hasPhysicalQuantity ?q2. 
	 ?q2 a ?qType;
	        lis:qualityQuantifiedAs ?d2.  
	 ?d2 lis:datumValue ?v2.  
	 }
	}
	}
