select ?node_name ?aspect ?max (SUM(?value) AS ?vsum) ?qTypeName where {\
                ?n a imf:AspectObject; \
                        imf:hasAspect ?aspect;\
                        rdfs:label ?node_name; \
                        lis:hasPhysicalQuantity ?q1. \
                ?q1 a ?qType, mimir:SummablePhysicalQuantity; \
                        lis:qualityQuantifiedAs ?datum.\
                ?qType rdfs:label ?qTypeName; \
                        rdfs:subClassOf mimir:AttributeType. \
                FILTER NOT EXISTS {?qtypechild rdfs:subClassOf ?qType}. \
                ?datum lis:datumValue ?max. \
                ?n imf:hasChild ?c.\
                ?c lis:hasPhysicalQuantity ?qc. \
                ?qc a ?qType; \
                        lis:qualityQuantifiedAs ?cDatum. \
                ?cDatum lis:datumValue ?value.\
} GROUP BY ?n ?qType \
HAVING (SUM(?value) > ?max)


