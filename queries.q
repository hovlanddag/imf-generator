prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix mimir: <http://example.com/mimir#>
prefix imf: <http://example.com/imf#>


select * where {
  ?t1 a imf:OutTerminal.
  ?t1 imf:hasParent* ?t.
  ?t imf:connectedTo ?t2.  
  ?transport imf:hasTerminal ?t2; imf:hasTerminal ?t3.
  ?t3 imf:connectedTo ?t4.
  ?t4 a imf:InTerminal; imf:hasChild* ?t5.
 ?n2 imf:hasTerminal ?t4.
}

prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix mimir: <http://example.com/mimir#>
prefix imf: <http://example.com/imf#>


select * where {
  ?root a imf:IntegratedObject.
   ?aspect imf:isAspectOf ?root;
           imf:hasChild* ?node.
}

prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix mimir: <http://example.com/mimir#>
prefix imf: <http://example.com/imf#>


select * where {\
  ?n a imf:FunctionalSystemBlock. \
  ?n imf:hasNodeDepth ?dpt. \
  FILTER (?dpt < 7). \
   ?n imf:hasTerminal ?t1.\
  ?t1 a imf:OutTerminal.\
  ?t1 imf:connectedTo ?t2.\
  ?tr imf:hasTerminal ?t2, ?t3.\
  ?t2 a imf:InTerminal.\
  ?t3 a imf:OutTerminal;\
        imf:connectedTo ?t4.\
  ?n2 imf:hasTerminal ?t4.\
  ?n2 a imf:FunctionalSystemBlock.\
}