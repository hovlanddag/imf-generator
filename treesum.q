select ?node_name ?aspect ?max (SUM(?value) AS ?vsum) ?qTypeName where {\
                ?n a imf:AspectObject; \
                imf:hasAspect ?aspect;\
                rdfs:label ?node_name; \
                lis:hasPhysicalQuantity ?q1. \
                ?q1 a ?qType. \
                ?qType rdfs:label ?qTypeName. \
                #FILTER NOT EXISTS {?qtypechild rdfs:subClassOf ?qType}. \
                ?q1 a mimir:SummablePhysicalQuantity.\
                ?q1 lis:qualityQuantifiedAs ?datum.\
                ?datum lis:datumValue ?max. \
                        ?n imf:hasChild+ ?c.\
                        FILTER NOT EXISTS {?c imf:hasChild ?cc.} \ 
                        ?c lis:hasPhysicalQuantity ?qc. \
                        ?qc a ?qType; \
                        lis:qualityQuantifiedAs ?cDatum. \
                        ?cDatum lis:datumValue ?value.\
} GROUP BY ?n ?qType \
HAVING (SUM(?value) > ?max)


