dstore delete imfExample force
dstore create imfExample type par-complex-nn equality off
active imfExample
clear force

set query.answer-format text/x.tab-separated-values-abbrev
#set query.answer-format application/n-triples
set output out

prefix imf: <http://example.com/imf#>
prefix mimir: <http://example.com/mimir#>
prefix og2: <http://example.com/rds/og2#>
prefix og1: <http://example.com/rds/og1#>
prefix : <http://example.com/generator/imfExample#>
PREFIX rdfox: <http://oxfordsemantic.tech/RDFox#>


import > :axioms ontologies/imf-202109.owl
#import > :axioms ontologies/rdscodes_gen.ttl
#import > :axioms ontologies/rds-202109.owl
import > :axioms ontologies/mimir-202110.owl
#import > :axioms ontologies/provenance.owl
#import > :prov ontologies/prov-o-20130430.ttl
importaxioms :axioms

import dybde16.ttl


