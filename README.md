# IMF Generator

For generating IMF data

Usage: 
Edit depth in file generator.dlog
Install RDFOx

run on commandline: RDFOx sandbox
run in RDFOx terminal:
enpoint start
generator.txt

To store ontology, run again in RDFOx terminal
export "filename.ttl" application/n-triples fact-domain IDB
