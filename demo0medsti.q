prefix imf: <http://ns.imfid.org/imf#> 
prefix lis: <http://standards.iso.org/iso/15926/part14/> 
prefix owl: <http://www.w3.org/2002/07/owl#> 
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
prefix sor: <https://rdf.equinor.com/sor/mimir/> 
prefix xml: <http://www.w3.org/XML/1998/namespace> 
prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
prefix mimir: <http://ns.imfid.org/mimir#> 


SELECT * WHERE {
    ?n rdfs:label "A-80EH001A 11kV Switchgear".
    ?n imf:hasParent* ?p.
   OPTIONAL{ ?p imf:isAspectOf ?project.}
}